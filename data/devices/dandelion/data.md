---
variantOf: "angelica"
name: "Xiaomi Redmi 9A"
price:
  min: 95
  max: 150

deviceInfo:
  - id: "releaseDate"
    value: "July 07 2020"
---
