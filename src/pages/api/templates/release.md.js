import { empty } from "empty-schema";
import YAML from "js-yaml";
import { releaseRules } from "@data/validationRules.js";
import { portStatusAsArray } from "@logic/build/dataUtils.js";

const rules = releaseRules();

export async function get() {
  const data = empty(rules);
  data.portStatus = portStatusAsArray(data.portStatus);

  return {
    body:
      "---\n" + YAML.dump(data, { forceQuotes: true, quotingType: '"' }) + "---"
  };
}
