---
variantOf: "marlin"
name: "Google Pixel"

deviceInfo:
  - id: "display"
    value: "AMOLED 1080x1920 5in"
---
